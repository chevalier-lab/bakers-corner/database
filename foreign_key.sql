
ALTER TABLE `m_icons`
ADD INDEX `fk_mi_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_mi_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `m_categories`
ADD INDEX `fk_mc_imi` (`id_m_icons` ASC),
ADD CONSTRAINT `fk_mc_imi`
  FOREIGN KEY (`id_m_icons`)
  REFERENCES `m_icons` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `m_users`
ADD INDEX `fk_mu_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_mu_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `m_users`
ADD INDEX `fk_mu_iml` (`id_m_level` ASC),
ADD CONSTRAINT `fk_mu_iml`
  FOREIGN KEY (`id_m_level`)
  REFERENCES `m_level` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `m_logs`
ADD INDEX `fk_mls_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_mls_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `m_settings`
ADD INDEX `fk_ms_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_ms_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_chat_user`
ADD INDEX `fk_tcu_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tcu_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_chat_detail_user`
ADD INDEX `fk_tcdu_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tcdu_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_chat_detail_user`
ADD INDEX `fk_tcdu_itcu` (`id_t_chat_user` ASC),
ADD CONSTRAINT `fk_tcdu_itcu`
  FOREIGN KEY (`id_t_chat_user`)
  REFERENCES `t_chat_user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_notification`
ADD INDEX `fk_tn_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tn_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_products`
ADD INDEX `fk_tp_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_tp_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `u_product_photos`
ADD INDEX `fk_upp_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_upp_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `u_product_photos`
ADD INDEX `fk_upp_tp` (`id_t_products` ASC),
ADD CONSTRAINT `fk_upp_tp`
  FOREIGN KEY (`id_t_products`)
  REFERENCES `t_products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_feedback_product`
ADD INDEX `fk_tfp_tp` (`id_t_products` ASC),
ADD CONSTRAINT `fk_tfp_tp`
  FOREIGN KEY (`id_t_products`)
  REFERENCES `t_products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_feedback_product`
ADD INDEX `fk_tfp_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tfp_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_voucher`
ADD INDEX `fk_tv_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_tv_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_voucher_redeem_user`
ADD INDEX `fk_tvru_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tvru_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_voucher_redeem_user`
ADD INDEX `fk_tvru_itv` (`id_t_voucher` ASC),
ADD CONSTRAINT `fk_tvru_itv`
  FOREIGN KEY (`id_t_voucher`)
  REFERENCES `t_voucher` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_request_menu`
ADD INDEX `fk_trm_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_trm_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_banner`
ADD INDEX `fk_tb_imm` (`id_m_medias` ASC),
ADD CONSTRAINT `fk_tb_imm`
  FOREIGN KEY (`id_m_medias`)
  REFERENCES `m_medias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_cart_product_user`
ADD INDEX `fk_tcpu_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tcpu_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_cart_product_user`
ADD INDEX `fk_tcpu_itp` (`id_t_products` ASC),
ADD CONSTRAINT `fk_tcpu_itp`
  FOREIGN KEY (`id_t_products`)
  REFERENCES `t_products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_cart_request_menu_user`
ADD INDEX `fk_tcrmu_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_tcrmu_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_cart_request_menu_user`
ADD INDEX `fk_tcrmu_itrm` (`id_t_request_menu` ASC),
ADD CONSTRAINT `fk_tcrmu_itrm`
  FOREIGN KEY (`id_t_request_menu`)
  REFERENCES `t_request_menu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_wishlist_product`
ADD INDEX `fk_twp_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_twp_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_wishlist_product`
ADD INDEX `fk_twp_itp` (`id_t_products` ASC),
ADD CONSTRAINT `fk_twp_itp`
  FOREIGN KEY (`id_t_products`)
  REFERENCES `t_products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_product`
ADD INDEX `fk_ttp_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_ttp_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_request_menu`
ADD INDEX `fk_ttrm_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_ttrm_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_product_item`
ADD INDEX `fk_ttpi_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_ttpi_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_product_item`
ADD INDEX `fk_ttpi_ittp` (`id_t_transaction_product` ASC),
ADD CONSTRAINT `fk_ttpi_ittp`
  FOREIGN KEY (`id_t_transaction_product`)
  REFERENCES `t_transaction_product` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_product_item`
ADD INDEX `fk_ttpi_itp` (`id_t_products` ASC),
ADD CONSTRAINT `fk_ttpi_itp`
  FOREIGN KEY (`id_t_products`)
  REFERENCES `t_products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_request_menu_item`
ADD INDEX `fk_ttrmi_imu` (`id_m_users` ASC),
ADD CONSTRAINT `fk_ttrmi_imu`
  FOREIGN KEY (`id_m_users`)
  REFERENCES `m_users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_request_menu_item`
ADD INDEX `fk_ttrmi_ittrm` (`id_t_transaction_request_menu` ASC),
ADD CONSTRAINT `fk_ttrmi_ittrm`
  FOREIGN KEY (`id_t_transaction_request_menu`)
  REFERENCES `t_transaction_request_menu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `t_transaction_request_menu_item`
ADD INDEX `fk_ttrmi_itrm` (`id_t_request_menu` ASC),
ADD CONSTRAINT `fk_ttrmi_itrm`
  FOREIGN KEY (`id_t_request_menu`)
  REFERENCES `t_request_menu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;